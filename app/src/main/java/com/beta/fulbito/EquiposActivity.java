package com.beta.fulbito;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EquiposActivity extends AppCompatActivity {

    private TextView team1, team2;
    private List<Jugador> listaEquipoBlanco;
    private List<Jugador> listaEquipoNegro;
    private ListView listViewEquipoBlanco;
    private ListView listViewEquipoNegro;
    private ListViewEquiposAdapter listViewEquipoBlancoAdapter;
    private ListViewEquiposAdapter listViewEquipoNegroAdapter;

    private EditText mEditBlanco;
    private EditText mEditNegro;

    private DatabaseReference mDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipos);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("jugadores");

        listaEquipoBlanco = new ArrayList<>();
        listaEquipoNegro = new ArrayList<>();

        team1 = (TextView) this.findViewById(R.id.TextView01);
        team2 = (TextView) this.findViewById(R.id.TextView02);
        listViewEquipoBlanco = (ListView) findViewById(R.id.listview_equipo_blanco);
        listViewEquipoNegro = (ListView) findViewById(R.id.listview_equipo_negro);

        //Randomizo el orden de la lista de jugadores
        Collections.shuffle(MainActivity.listaDeJugadoresSeleccionados);

        calcTeams(MainActivity.listaDeJugadoresSeleccionados);
        MainActivity.listaDeJugadoresSeleccionados.clear();

        /*para armar equipos a mano (anular el calcTeams):
        listaEquipoBlanco.add(new Jugador("Pata",72,1,R.drawable.pata));
        listaEquipoBlanco.add(new Jugador("Beta",78,1,R.drawable.beta));
        listaEquipoBlanco.add(new Jugador("Juano",62,1,R.drawable.juano));
        listaEquipoBlanco.add(new Jugador("Guido",70,1,R.drawable.guido));
        listaEquipoBlanco.add(new Jugador("Maxi",63,1,R.drawable.maxi));
        listaEquipoNegro.add(new Jugador("Lucas G",53,1,R.drawable.lucas_g));
        listaEquipoNegro.add(new Jugador("Gaita",71,1,R.drawable.gaita));
        listaEquipoNegro.add(new Jugador("Uruguayo",60,1,R.drawable.uruguayo));
        listaEquipoNegro.add(new Jugador("Nano",85,1,R.drawable.nano));
        listaEquipoNegro.add(new Jugador("Toto",74,1,R.drawable.toto));

        team1.setText("Equipo RDM (Blanco): " + (listaEquipoBlanco.get(0).getPuntaje() + listaEquipoBlanco.get(1).getPuntaje() + listaEquipoBlanco.get(2).getPuntaje()
                + listaEquipoBlanco.get(3).getPuntaje()+ listaEquipoBlanco.get(4).getPuntaje()));
        team2.setText("Equipo UTDT (Negro): " + (listaEquipoNegro.get(0).getPuntaje() + listaEquipoNegro.get(1).getPuntaje() + listaEquipoNegro.get(2).getPuntaje()
                + listaEquipoNegro.get(3).getPuntaje()+ listaEquipoNegro.get(4).getPuntaje()));*/
        

        listViewEquipoBlancoAdapter = new ListViewEquiposAdapter(this, listaEquipoBlanco);
        listViewEquipoBlanco.setAdapter(listViewEquipoBlancoAdapter);
        listViewEquipoNegroAdapter = new ListViewEquiposAdapter(this, listaEquipoNegro);
        listViewEquipoNegro.setAdapter(listViewEquipoNegroAdapter);
    }

    public void calcTeams(List<Jugador> listaDeJugadores) {
        int jugador1 = 0;
        int jugador2 = 1;
        int jugador3 = 2;
        int jugador4 = 3;
        int jugador5 = 4;
        int jugador6 = 5;
        int jugador7 = 6;
        int jugador8 = 7;
        int jugador9 = 8;
        int jugador10 = 9;
        int jugador11 = 10;
        int jugador12 = 11;
        int jugador13 = 12;
        int jugador14 = 13;
        int jugador15 = 14;
        int jugador16 = 15;
        int jugador17 = 16;
        int jugador18 = 17;
        int jugador19 = 18;
        int jugador20 = 19;
        int jugador21 = 20;
        int jugador22 = 21;
        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        int e = 0;
        int f = 0;
        int g = 0;
        int h = 0;
        int i = 0;
        int j = 0;
        int k = 0;

        //Para 4 jugadores
        if (listaDeJugadores.size() == 4) {
            int dif = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(1).getPuntaje()) - (listaDeJugadores.get(2).getPuntaje()
                    + listaDeJugadores.get(3).getPuntaje()));
            for (int cont2 = 1; cont2 < listaDeJugadores.size(); cont2++) {
                    int iterador[] = {1, 2, 3};
                    int jj = 1;
                    for (int ii = 0; ii < iterador.length; ii++) {
                        if (cont2 == iterador[ii]) {
                            jj++;
                        } else {
                            b = a;
                            a = jj;
                            jj++;
                        }
                    }

                    int dif1 = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(cont2).getPuntaje())
                            - (listaDeJugadores.get(a).getPuntaje() + listaDeJugadores.get(b).getPuntaje()));
                    if (dif1 < dif) {
                        jugador1 = 0;
                        jugador2 = cont2;
                        jugador3 = a;
                        jugador4 = b;
                        dif = dif1;
                    }
            }
            listaEquipoBlanco.add(listaDeJugadores.get(jugador1));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador2));
            listaEquipoNegro.add(listaDeJugadores.get(jugador3));
            listaEquipoNegro.add(listaDeJugadores.get(jugador4));

            Collections.sort(listaEquipoBlanco, new SortByScore());
            Collections.sort(listaEquipoNegro, new SortByScore());

            team1.setText("Equipo Blanco: " + (listaDeJugadores.get(jugador1).getPuntaje() + listaDeJugadores.get(jugador2).getPuntaje()));
            team2.setText("Equipo Negro: " + (listaDeJugadores.get(jugador3).getPuntaje() + listaDeJugadores.get(jugador4).getPuntaje()));
        }

        //Para 6 jugadores
        else if (listaDeJugadores.size() == 6) {
            int dif = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(1).getPuntaje() + listaDeJugadores.get(2).getPuntaje())
                    - (listaDeJugadores.get(3).getPuntaje() + listaDeJugadores.get(4).getPuntaje() + listaDeJugadores.get(5).getPuntaje()));
            for (int cont2 = 1; cont2 < listaDeJugadores.size() - 1; cont2++) {
                for (int cont3 = cont2 + 1; cont3 < listaDeJugadores.size(); cont3++) {
                    int iterador[] = {1, 2, 3, 4, 5};
                    int jj = 1;
                    for (int ii = 0; ii < iterador.length; ii++) {
                        if (cont2 == iterador[ii] | cont3 == iterador[ii]) {
                            jj++;
                        } else {
                            c = b;
                            b = a;
                            a = jj;
                            jj++;
                        }
                    }

                    int dif1 = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(cont2).getPuntaje() + listaDeJugadores.get(cont3).getPuntaje())
                            - (listaDeJugadores.get(a).getPuntaje() + listaDeJugadores.get(b).getPuntaje() + listaDeJugadores.get(c).getPuntaje()));
                    if (dif1 < dif) {
                        jugador1 = 0;
                        jugador2 = cont2;
                        jugador3 = cont3;
                        jugador4 = a;
                        jugador5 = b;
                        jugador6 = c;
                        dif = dif1;
                    }
                }
            }
            listaEquipoBlanco.add(listaDeJugadores.get(jugador1));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador2));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador3));
            listaEquipoNegro.add(listaDeJugadores.get(jugador4));
            listaEquipoNegro.add(listaDeJugadores.get(jugador5));
            listaEquipoNegro.add(listaDeJugadores.get(jugador6));

            Collections.sort(listaEquipoBlanco, new SortByScore());
            Collections.sort(listaEquipoNegro, new SortByScore());

            team1.setText("Equipo Blanco: " + (listaEquipoBlanco.get(0).getPuntaje() + listaEquipoBlanco.get(1).getPuntaje() + listaEquipoBlanco.get(2).getPuntaje()));
            team2.setText("Equipo Negro: " + (listaEquipoNegro.get(0).getPuntaje() + listaEquipoNegro.get(1).getPuntaje() + listaEquipoNegro.get(2).getPuntaje()));
        }

        //Para 8 jugadores
        else if (listaDeJugadores.size() == 8) {
            int dif = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(1).getPuntaje()
                    + listaDeJugadores.get(2).getPuntaje() + listaDeJugadores.get(3).getPuntaje())
                    - (listaDeJugadores.get(4).getPuntaje() + listaDeJugadores.get(5).getPuntaje()
                    + listaDeJugadores.get(6).getPuntaje() + listaDeJugadores.get(7).getPuntaje()));

            for (int cont2 = 1; cont2 < listaDeJugadores.size() - 2; cont2++) {
                for (int cont3 = cont2 + 1; cont3 < listaDeJugadores.size() - 1; cont3++) {
                    for (int cont4 = cont3 + 1; cont4 < listaDeJugadores.size(); cont4++) {
                        int iterador[] = {1, 2, 3, 4, 5, 6, 7};
                        int jj = 1;
                        for (int ii = 0; ii < iterador.length; ii++) {
                            if (cont2 == iterador[ii] | cont3 == iterador[ii] | cont4 == iterador[ii]) {
                                jj++;
                            } else {
                                d = c;
                                c = b;
                                b = a;
                                a = jj;
                                jj++;
                            }
                        }

                        int dif1 = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(cont2).getPuntaje() + listaDeJugadores.get(cont3).getPuntaje() + listaDeJugadores.get(cont4).getPuntaje())
                                - (listaDeJugadores.get(a).getPuntaje() + listaDeJugadores.get(b).getPuntaje() + listaDeJugadores.get(c).getPuntaje() + listaDeJugadores.get(d).getPuntaje()));
                        if (dif1 < dif) {
                            jugador1 = 0;
                            jugador2 = cont2;
                            jugador3 = cont3;
                            jugador4 = cont4;
                            jugador5 = a;
                            jugador6 = b;
                            jugador7 = c;
                            jugador8 = d;
                            dif = dif1;
                        }
                    }
                }
            }
            listaEquipoBlanco.add(listaDeJugadores.get(jugador1));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador2));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador3));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador4));
            listaEquipoNegro.add(listaDeJugadores.get(jugador5));
            listaEquipoNegro.add(listaDeJugadores.get(jugador6));
            listaEquipoNegro.add(listaDeJugadores.get(jugador7));
            listaEquipoNegro.add(listaDeJugadores.get(jugador8));

            Collections.sort(listaEquipoBlanco, new SortByScore());
            Collections.sort(listaEquipoNegro, new SortByScore());

            team1.setText("Equipo Blanco: " + (listaEquipoBlanco.get(0).getPuntaje() + listaEquipoBlanco.get(1).getPuntaje() + listaEquipoBlanco.get(2).getPuntaje() + listaEquipoBlanco.get(3).getPuntaje()));
            team2.setText("Equipo Negro: " + (listaEquipoNegro.get(0).getPuntaje() + listaEquipoNegro.get(1).getPuntaje() + listaEquipoNegro.get(2).getPuntaje() + listaEquipoNegro.get(3).getPuntaje()));
        }

        //Para 10 jugadores
        else if (listaDeJugadores.size() == 10) {
            int dif = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(1).getPuntaje()
                    + listaDeJugadores.get(2).getPuntaje() + listaDeJugadores.get(3).getPuntaje()+ listaDeJugadores.get(4).getPuntaje())
                    - (listaDeJugadores.get(5).getPuntaje() + listaDeJugadores.get(6).getPuntaje()
                    + listaDeJugadores.get(7).getPuntaje() + listaDeJugadores.get(8).getPuntaje()+ listaDeJugadores.get(9).getPuntaje()));

            for (int cont2 = 1; cont2 < listaDeJugadores.size() - 3; cont2++) {
                for (int cont3 = cont2 + 1; cont3 < listaDeJugadores.size() - 2; cont3++) {
                    for (int cont4 = cont3 + 1; cont4 < listaDeJugadores.size() - 1; cont4++) {
                        for (int cont5 = cont4 + 1; cont5 < listaDeJugadores.size(); cont5++) {
                            int iterador[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
                            int jj = 1;
                            for (int ii = 0; ii < iterador.length; ii++) {
                                if (cont2 == iterador[ii] | cont3 == iterador[ii] | cont4 == iterador[ii] | cont5 == iterador[ii]) {
                                    jj++;
                                } else {
                                    e = d;
                                    d = c;
                                    c = b;
                                    b = a;
                                    a = jj;
                                    jj++;
                                }
                            }

                            int dif1 = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(cont2).getPuntaje() + listaDeJugadores.get(cont3).getPuntaje()
                                    + listaDeJugadores.get(cont4).getPuntaje() + listaDeJugadores.get(cont5).getPuntaje())
                                    - (listaDeJugadores.get(a).getPuntaje() + listaDeJugadores.get(b).getPuntaje() + listaDeJugadores.get(c).getPuntaje()
                                    + listaDeJugadores.get(d).getPuntaje() + listaDeJugadores.get(e).getPuntaje()));
                            if (dif1 < dif) {
                                jugador1 = 0;
                                jugador2 = cont2;
                                jugador3 = cont3;
                                jugador4 = cont4;
                                jugador5 = cont5;
                                jugador6 = a;
                                jugador7 = b;
                                jugador8 = c;
                                jugador9 = d;
                                jugador10 = e;
                                dif = dif1;
                            }
                        }
                    }
                }
            }
            listaEquipoBlanco.add(listaDeJugadores.get(jugador1));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador2));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador3));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador4));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador5));
            listaEquipoNegro.add(listaDeJugadores.get(jugador6));
            listaEquipoNegro.add(listaDeJugadores.get(jugador7));
            listaEquipoNegro.add(listaDeJugadores.get(jugador8));
            listaEquipoNegro.add(listaDeJugadores.get(jugador9));
            listaEquipoNegro.add(listaDeJugadores.get(jugador10));

            Collections.sort(listaEquipoBlanco, new SortByScore());
            Collections.sort(listaEquipoNegro, new SortByScore());

            team1.setText("Equipo Blanco: " + (listaEquipoBlanco.get(0).getPuntaje() + listaEquipoBlanco.get(1).getPuntaje() + listaEquipoBlanco.get(2).getPuntaje()
                    + listaEquipoBlanco.get(3).getPuntaje()+ listaEquipoBlanco.get(4).getPuntaje()));
            team2.setText("Equipo Negro: " + (listaEquipoNegro.get(0).getPuntaje() + listaEquipoNegro.get(1).getPuntaje() + listaEquipoNegro.get(2).getPuntaje()
                    + listaEquipoNegro.get(3).getPuntaje()+ listaEquipoNegro.get(4).getPuntaje()));
        }

        //Para 12 jugadores
        else if (listaDeJugadores.size() == 12) {
            int dif = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(1).getPuntaje() + listaDeJugadores.get(2).getPuntaje()
                    + listaDeJugadores.get(3).getPuntaje() + listaDeJugadores.get(4).getPuntaje() + listaDeJugadores.get(5).getPuntaje())
                    - (listaDeJugadores.get(6).getPuntaje() + listaDeJugadores.get(7).getPuntaje() + listaDeJugadores.get(8).getPuntaje()
                    + listaDeJugadores.get(9).getPuntaje() + listaDeJugadores.get(10).getPuntaje() + listaDeJugadores.get(11).getPuntaje()));

            for (int cont2 = 1; cont2 < listaDeJugadores.size() - 4; cont2++) {
                for (int cont3 = cont2 + 1; cont3 < listaDeJugadores.size() - 3; cont3++) {
                    for (int cont4 = cont3 + 1; cont4 < listaDeJugadores.size() - 2; cont4++) {
                        for (int cont5 = cont4 + 1; cont5 < listaDeJugadores.size() - 1; cont5++) {
                            for (int cont6 = cont5 + 1; cont6 < listaDeJugadores.size(); cont6++) {
                                int iterador[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
                                int jj = 1;
                                for (int ii = 0; ii < iterador.length; ii++) {
                                    if (cont2 == iterador[ii] | cont3 == iterador[ii] | cont4 == iterador[ii] | cont5 == iterador[ii]| cont6 == iterador[ii]) {
                                        jj++;
                                    } else {
                                        f = e;
                                        e = d;
                                        d = c;
                                        c = b;
                                        b = a;
                                        a = jj;
                                        jj++;
                                    }
                                }

                                int dif1 = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(cont2).getPuntaje() + listaDeJugadores.get(cont3).getPuntaje()
                                        + listaDeJugadores.get(cont4).getPuntaje() + listaDeJugadores.get(cont5).getPuntaje()+ listaDeJugadores.get(cont6).getPuntaje())
                                        - (listaDeJugadores.get(a).getPuntaje() + listaDeJugadores.get(b).getPuntaje() + listaDeJugadores.get(c).getPuntaje()
                                        + listaDeJugadores.get(d).getPuntaje() + listaDeJugadores.get(e).getPuntaje()+ listaDeJugadores.get(f).getPuntaje()));
                                if (dif1 < dif) {
                                    jugador1 = 0;
                                    jugador2 = cont2;
                                    jugador3 = cont3;
                                    jugador4 = cont4;
                                    jugador5 = cont5;
                                    jugador6 = cont6;
                                    jugador7 = a;
                                    jugador8 = b;
                                    jugador9 = c;
                                    jugador10 = d;
                                    jugador11 = e;
                                    jugador12 = f;
                                    dif = dif1;
                                }
                            }
                        }
                    }
                }
            }
            listaEquipoBlanco.add(listaDeJugadores.get(jugador1));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador2));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador3));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador4));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador5));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador6));
            listaEquipoNegro.add(listaDeJugadores.get(jugador7));
            listaEquipoNegro.add(listaDeJugadores.get(jugador8));
            listaEquipoNegro.add(listaDeJugadores.get(jugador9));
            listaEquipoNegro.add(listaDeJugadores.get(jugador10));
            listaEquipoNegro.add(listaDeJugadores.get(jugador11));
            listaEquipoNegro.add(listaDeJugadores.get(jugador12));

            Collections.sort(listaEquipoBlanco, new SortByScore());
            Collections.sort(listaEquipoNegro, new SortByScore());


            team1.setText("Equipo Blanco: " + (listaEquipoBlanco.get(0).getPuntaje() + listaEquipoBlanco.get(1).getPuntaje() + listaEquipoBlanco.get(2).getPuntaje()
                    + listaEquipoBlanco.get(3).getPuntaje() + listaEquipoBlanco.get(4).getPuntaje()+ listaEquipoBlanco.get(5).getPuntaje()));
            team2.setText("Equipo Negro: " + (listaEquipoNegro.get(0).getPuntaje() + listaEquipoNegro.get(1).getPuntaje() + listaEquipoNegro.get(2).getPuntaje()
                    + listaEquipoNegro.get(3).getPuntaje() + listaEquipoNegro.get(4).getPuntaje()+ listaEquipoNegro.get(5).getPuntaje()));
        }

        //Para 14 jugadores
        else if (listaDeJugadores.size() == 14) {
            int dif = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(1).getPuntaje() + listaDeJugadores.get(2).getPuntaje()
                    + listaDeJugadores.get(3).getPuntaje() + listaDeJugadores.get(4).getPuntaje() + listaDeJugadores.get(5).getPuntaje()+ listaDeJugadores.get(6).getPuntaje())
                    - (listaDeJugadores.get(7).getPuntaje() + listaDeJugadores.get(8).getPuntaje() + listaDeJugadores.get(9).getPuntaje()
                    + listaDeJugadores.get(10).getPuntaje() + listaDeJugadores.get(11).getPuntaje() + listaDeJugadores.get(12).getPuntaje()+ listaDeJugadores.get(13).getPuntaje()));

            for (int cont2 = 1; cont2 < listaDeJugadores.size() - 4; cont2++) {
                for (int cont3 = cont2 + 1; cont3 < listaDeJugadores.size() - 3; cont3++) {
                    for (int cont4 = cont3 + 1; cont4 < listaDeJugadores.size() - 2; cont4++) {
                        for (int cont5 = cont4 + 1; cont5 < listaDeJugadores.size() - 1; cont5++) {
                            for (int cont6 = cont5 + 1; cont6 < listaDeJugadores.size(); cont6++) {
                                for (int cont7 = cont6 + 1; cont7 < listaDeJugadores.size(); cont7++) {
                                    int iterador[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
                                    int jj = 1;
                                    for (int ii = 0; ii < iterador.length; ii++) {
                                        if (cont2 == iterador[ii] | cont3 == iterador[ii] | cont4 == iterador[ii] | cont5 == iterador[ii] | cont6 == iterador[ii] | cont7 == iterador[ii]) {
                                            jj++;
                                        } else {
                                            g = f;
                                            f = e;
                                            e = d;
                                            d = c;
                                            c = b;
                                            b = a;
                                            a = jj;
                                            jj++;
                                        }
                                    }

                                    int dif1 = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(cont2).getPuntaje() + listaDeJugadores.get(cont3).getPuntaje()
                                            + listaDeJugadores.get(cont4).getPuntaje() + listaDeJugadores.get(cont5).getPuntaje() + listaDeJugadores.get(cont6).getPuntaje() + listaDeJugadores.get(cont7).getPuntaje())
                                            - (listaDeJugadores.get(a).getPuntaje() + listaDeJugadores.get(b).getPuntaje() + listaDeJugadores.get(c).getPuntaje()
                                            + listaDeJugadores.get(d).getPuntaje() + listaDeJugadores.get(e).getPuntaje() + listaDeJugadores.get(f).getPuntaje() + listaDeJugadores.get(g).getPuntaje()));
                                    if (dif1 < dif) {
                                        jugador1 = 0;
                                        jugador2 = cont2;
                                        jugador3 = cont3;
                                        jugador4 = cont4;
                                        jugador5 = cont5;
                                        jugador6 = cont6;
                                        jugador7 = cont7;
                                        jugador8 = a;
                                        jugador9 = b;
                                        jugador10 = c;
                                        jugador11 = d;
                                        jugador12 = e;
                                        jugador13 = f;
                                        jugador14 = g;
                                        dif = dif1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            listaEquipoBlanco.add(listaDeJugadores.get(jugador1));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador2));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador3));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador4));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador5));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador6));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador7));
            listaEquipoNegro.add(listaDeJugadores.get(jugador8));
            listaEquipoNegro.add(listaDeJugadores.get(jugador9));
            listaEquipoNegro.add(listaDeJugadores.get(jugador10));
            listaEquipoNegro.add(listaDeJugadores.get(jugador11));
            listaEquipoNegro.add(listaDeJugadores.get(jugador12));
            listaEquipoNegro.add(listaDeJugadores.get(jugador13));
            listaEquipoNegro.add(listaDeJugadores.get(jugador14));

            Collections.sort(listaEquipoBlanco, new SortByScore());
            Collections.sort(listaEquipoNegro, new SortByScore());


            team1.setText("Equipo Blanco: " + (listaEquipoBlanco.get(0).getPuntaje() + listaEquipoBlanco.get(1).getPuntaje() + listaEquipoBlanco.get(2).getPuntaje()
                    + listaEquipoBlanco.get(3).getPuntaje() + listaEquipoBlanco.get(4).getPuntaje()+ listaEquipoBlanco.get(5).getPuntaje()+ listaEquipoBlanco.get(6).getPuntaje()));
            team2.setText("Equipo Negro: " + (listaEquipoNegro.get(0).getPuntaje() + listaEquipoNegro.get(1).getPuntaje() + listaEquipoNegro.get(2).getPuntaje()
                    + listaEquipoNegro.get(3).getPuntaje() + listaEquipoNegro.get(4).getPuntaje()+ listaEquipoNegro.get(5).getPuntaje()+ listaEquipoNegro.get(6).getPuntaje()));
        }

        //Para 16 jugadores
        else if (listaDeJugadores.size() == 16) {
            int dif = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(1).getPuntaje() + listaDeJugadores.get(2).getPuntaje() + listaDeJugadores.get(3).getPuntaje()
                    + listaDeJugadores.get(4).getPuntaje() + listaDeJugadores.get(5).getPuntaje()+ listaDeJugadores.get(6).getPuntaje()+ listaDeJugadores.get(7).getPuntaje())
                    - (listaDeJugadores.get(8).getPuntaje() + listaDeJugadores.get(9).getPuntaje() + listaDeJugadores.get(10).getPuntaje() + listaDeJugadores.get(11).getPuntaje()
                    + listaDeJugadores.get(12).getPuntaje() + listaDeJugadores.get(13).getPuntaje()+ listaDeJugadores.get(14).getPuntaje()+ listaDeJugadores.get(15).getPuntaje()));

            for (int cont2 = 1; cont2 < listaDeJugadores.size() - 4; cont2++) {
                for (int cont3 = cont2 + 1; cont3 < listaDeJugadores.size() - 3; cont3++) {
                    for (int cont4 = cont3 + 1; cont4 < listaDeJugadores.size() - 2; cont4++) {
                        for (int cont5 = cont4 + 1; cont5 < listaDeJugadores.size() - 1; cont5++) {
                            for (int cont6 = cont5 + 1; cont6 < listaDeJugadores.size(); cont6++) {
                                for (int cont7 = cont6 + 1; cont7 < listaDeJugadores.size(); cont7++) {
                                    for (int cont8 = cont7 + 1; cont8 < listaDeJugadores.size(); cont8++) {
                                        int iterador[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
                                        int jj = 1;
                                        for (int ii = 0; ii < iterador.length; ii++) {
                                            if (cont2 == iterador[ii] | cont3 == iterador[ii] | cont4 == iterador[ii] | cont5 == iterador[ii] | cont6 == iterador[ii] | cont7 == iterador[ii]
                                                    | cont8 == iterador[ii]) {
                                                jj++;
                                            } else {
                                                h = g;
                                                g = f;
                                                f = e;
                                                e = d;
                                                d = c;
                                                c = b;
                                                b = a;
                                                a = jj;
                                                jj++;
                                            }
                                        }

                                        int dif1 = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(cont2).getPuntaje() + listaDeJugadores.get(cont3).getPuntaje()
                                                + listaDeJugadores.get(cont4).getPuntaje() + listaDeJugadores.get(cont5).getPuntaje() + listaDeJugadores.get(cont6).getPuntaje()
                                                + listaDeJugadores.get(cont7).getPuntaje() + listaDeJugadores.get(cont8).getPuntaje())
                                                - (listaDeJugadores.get(a).getPuntaje() + listaDeJugadores.get(b).getPuntaje() + listaDeJugadores.get(c).getPuntaje()
                                                + listaDeJugadores.get(d).getPuntaje() + listaDeJugadores.get(e).getPuntaje() + listaDeJugadores.get(f).getPuntaje()
                                                + listaDeJugadores.get(g).getPuntaje() + listaDeJugadores.get(h).getPuntaje()));
                                        if (dif1 < dif) {
                                            jugador1 = 0;
                                            jugador2 = cont2;
                                            jugador3 = cont3;
                                            jugador4 = cont4;
                                            jugador5 = cont5;
                                            jugador6 = cont6;
                                            jugador7 = cont7;
                                            jugador8 = cont8;
                                            jugador9 = a;
                                            jugador10 = b;
                                            jugador11 = c;
                                            jugador12 = d;
                                            jugador13 = e;
                                            jugador14 = f;
                                            jugador15 = g;
                                            jugador16 = h;
                                            dif = dif1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            listaEquipoBlanco.add(listaDeJugadores.get(jugador1));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador2));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador3));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador4));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador5));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador6));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador7));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador8));
            listaEquipoNegro.add(listaDeJugadores.get(jugador9));
            listaEquipoNegro.add(listaDeJugadores.get(jugador10));
            listaEquipoNegro.add(listaDeJugadores.get(jugador11));
            listaEquipoNegro.add(listaDeJugadores.get(jugador12));
            listaEquipoNegro.add(listaDeJugadores.get(jugador13));
            listaEquipoNegro.add(listaDeJugadores.get(jugador14));
            listaEquipoNegro.add(listaDeJugadores.get(jugador15));
            listaEquipoNegro.add(listaDeJugadores.get(jugador16));

            Collections.sort(listaEquipoBlanco, new SortByScore());
            Collections.sort(listaEquipoNegro, new SortByScore());


            team1.setText("Equipo Blanco: " + (listaEquipoBlanco.get(0).getPuntaje() + listaEquipoBlanco.get(1).getPuntaje() + listaEquipoBlanco.get(2).getPuntaje()
                    + listaEquipoBlanco.get(3).getPuntaje() + listaEquipoBlanco.get(4).getPuntaje()+ listaEquipoBlanco.get(5).getPuntaje()+ listaEquipoBlanco.get(6).getPuntaje()
                    + listaEquipoBlanco.get(7).getPuntaje()));
            team2.setText("Equipo Negro: " + (listaEquipoNegro.get(0).getPuntaje() + listaEquipoNegro.get(1).getPuntaje() + listaEquipoNegro.get(2).getPuntaje()
                    + listaEquipoNegro.get(3).getPuntaje() + listaEquipoNegro.get(4).getPuntaje()+ listaEquipoNegro.get(5).getPuntaje() + listaEquipoNegro.get(6).getPuntaje()
                    + listaEquipoNegro.get(7).getPuntaje()));
        }

        //Para 18 jugadores
        else if (listaDeJugadores.size() == 18) {
            int dif = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(1).getPuntaje() + listaDeJugadores.get(2).getPuntaje() + listaDeJugadores.get(3).getPuntaje()
                    + listaDeJugadores.get(4).getPuntaje() + listaDeJugadores.get(5).getPuntaje()+ listaDeJugadores.get(6).getPuntaje() + listaDeJugadores.get(7).getPuntaje()
                    + listaDeJugadores.get(8).getPuntaje())
                    - (listaDeJugadores.get(9).getPuntaje() + listaDeJugadores.get(10).getPuntaje() + listaDeJugadores.get(11).getPuntaje() + listaDeJugadores.get(12).getPuntaje()
                    + listaDeJugadores.get(13).getPuntaje() + listaDeJugadores.get(14).getPuntaje()+ listaDeJugadores.get(15).getPuntaje() + listaDeJugadores.get(16).getPuntaje()
                    + listaDeJugadores.get(17).getPuntaje()));

            for (int cont2 = 1; cont2 < listaDeJugadores.size() - 4; cont2++) {
                for (int cont3 = cont2 + 1; cont3 < listaDeJugadores.size() - 3; cont3++) {
                    for (int cont4 = cont3 + 1; cont4 < listaDeJugadores.size() - 2; cont4++) {
                        for (int cont5 = cont4 + 1; cont5 < listaDeJugadores.size() - 1; cont5++) {
                            for (int cont6 = cont5 + 1; cont6 < listaDeJugadores.size(); cont6++) {
                                for (int cont7 = cont6 + 1; cont7 < listaDeJugadores.size(); cont7++) {
                                    for (int cont8 = cont7 + 1; cont8 < listaDeJugadores.size(); cont8++) {
                                        for (int cont9 = cont8 + 1; cont9 < listaDeJugadores.size(); cont9++) {
                                            int iterador[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17};
                                            int jj = 1;
                                            for (int ii = 0; ii < iterador.length; ii++) {
                                                if (cont2 == iterador[ii] | cont3 == iterador[ii] | cont4 == iterador[ii] | cont5 == iterador[ii] | cont6 == iterador[ii]
                                                        | cont7 == iterador[ii] | cont8 == iterador[ii] | cont9 == iterador[ii]) {
                                                    jj++;
                                                } else {
                                                    i = h;
                                                    h = g;
                                                    g = f;
                                                    f = e;
                                                    e = d;
                                                    d = c;
                                                    c = b;
                                                    b = a;
                                                    a = jj;
                                                    jj++;
                                                }
                                            }

                                            int dif1 = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(cont2).getPuntaje() + listaDeJugadores.get(cont3).getPuntaje()
                                                    + listaDeJugadores.get(cont4).getPuntaje() + listaDeJugadores.get(cont5).getPuntaje() + listaDeJugadores.get(cont6).getPuntaje()
                                                    + listaDeJugadores.get(cont7).getPuntaje() + listaDeJugadores.get(cont8).getPuntaje() + listaDeJugadores.get(cont9).getPuntaje())
                                                    - (listaDeJugadores.get(a).getPuntaje() + listaDeJugadores.get(b).getPuntaje() + listaDeJugadores.get(c).getPuntaje()
                                                    + listaDeJugadores.get(d).getPuntaje() + listaDeJugadores.get(e).getPuntaje() + listaDeJugadores.get(f).getPuntaje()
                                                    + listaDeJugadores.get(g).getPuntaje() + listaDeJugadores.get(h).getPuntaje() + listaDeJugadores.get(i).getPuntaje()));
                                            if (dif1 < dif) {
                                                jugador1 = 0;
                                                jugador2 = cont2;
                                                jugador3 = cont3;
                                                jugador4 = cont4;
                                                jugador5 = cont5;
                                                jugador6 = cont6;
                                                jugador7 = cont7;
                                                jugador8 = cont8;
                                                jugador9 = cont9;
                                                jugador10 = a;
                                                jugador11 = b;
                                                jugador12 = c;
                                                jugador13 = d;
                                                jugador14 = e;
                                                jugador15 = f;
                                                jugador16 = g;
                                                jugador17 = h;
                                                jugador18 = i;
                                                dif = dif1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            listaEquipoBlanco.add(listaDeJugadores.get(jugador1));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador2));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador3));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador4));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador5));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador6));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador7));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador8));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador9));
            listaEquipoNegro.add(listaDeJugadores.get(jugador10));
            listaEquipoNegro.add(listaDeJugadores.get(jugador11));
            listaEquipoNegro.add(listaDeJugadores.get(jugador12));
            listaEquipoNegro.add(listaDeJugadores.get(jugador13));
            listaEquipoNegro.add(listaDeJugadores.get(jugador14));
            listaEquipoNegro.add(listaDeJugadores.get(jugador15));
            listaEquipoNegro.add(listaDeJugadores.get(jugador16));
            listaEquipoNegro.add(listaDeJugadores.get(jugador17));
            listaEquipoNegro.add(listaDeJugadores.get(jugador18));

            Collections.sort(listaEquipoBlanco, new SortByScore());
            Collections.sort(listaEquipoNegro, new SortByScore());


            team1.setText("Equipo Blanco: " + (listaEquipoBlanco.get(0).getPuntaje() + listaEquipoBlanco.get(1).getPuntaje() + listaEquipoBlanco.get(2).getPuntaje()
                    + listaEquipoBlanco.get(3).getPuntaje() + listaEquipoBlanco.get(4).getPuntaje()+ listaEquipoBlanco.get(5).getPuntaje()+ listaEquipoBlanco.get(6).getPuntaje()
                    + listaEquipoBlanco.get(7).getPuntaje()+ listaEquipoBlanco.get(8).getPuntaje()));
            team2.setText("Equipo Negro: " + (listaEquipoNegro.get(0).getPuntaje() + listaEquipoNegro.get(1).getPuntaje() + listaEquipoNegro.get(2).getPuntaje()
                    + listaEquipoNegro.get(3).getPuntaje() + listaEquipoNegro.get(4).getPuntaje()+ listaEquipoNegro.get(5).getPuntaje() + listaEquipoNegro.get(6).getPuntaje()
                    + listaEquipoNegro.get(7).getPuntaje() + listaEquipoNegro.get(8).getPuntaje()));
        }

        //Para 20 jugadores
        else if (listaDeJugadores.size() == 20) {
            int dif = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(1).getPuntaje() + listaDeJugadores.get(2).getPuntaje() + listaDeJugadores.get(3).getPuntaje()
                    + listaDeJugadores.get(4).getPuntaje() + listaDeJugadores.get(5).getPuntaje()+ listaDeJugadores.get(6).getPuntaje() + listaDeJugadores.get(7).getPuntaje()
                    + listaDeJugadores.get(8).getPuntaje() + listaDeJugadores.get(9).getPuntaje())
                    - (listaDeJugadores.get(10).getPuntaje() + listaDeJugadores.get(11).getPuntaje() + listaDeJugadores.get(12).getPuntaje() + listaDeJugadores.get(13).getPuntaje()
                    + listaDeJugadores.get(14).getPuntaje() + listaDeJugadores.get(15).getPuntaje()+ listaDeJugadores.get(16).getPuntaje() + listaDeJugadores.get(17).getPuntaje()
                    + listaDeJugadores.get(18).getPuntaje()+ listaDeJugadores.get(19).getPuntaje()));

            for (int cont2 = 1; cont2 < listaDeJugadores.size() - 4; cont2++) {
                for (int cont3 = cont2 + 1; cont3 < listaDeJugadores.size() - 3; cont3++) {
                    for (int cont4 = cont3 + 1; cont4 < listaDeJugadores.size() - 2; cont4++) {
                        for (int cont5 = cont4 + 1; cont5 < listaDeJugadores.size() - 1; cont5++) {
                            for (int cont6 = cont5 + 1; cont6 < listaDeJugadores.size(); cont6++) {
                                for (int cont7 = cont6 + 1; cont7 < listaDeJugadores.size(); cont7++) {
                                    for (int cont8 = cont7 + 1; cont8 < listaDeJugadores.size(); cont8++) {
                                        for (int cont9 = cont8 + 1; cont9 < listaDeJugadores.size(); cont9++) {
                                            for (int cont10 = cont9 + 1; cont10 < listaDeJugadores.size(); cont10++) {
                                                int iterador[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
                                                int jj = 1;
                                                for (int ii = 0; ii < iterador.length; ii++) {
                                                    if (cont2 == iterador[ii] | cont3 == iterador[ii] | cont4 == iterador[ii] | cont5 == iterador[ii] | cont6 == iterador[ii]
                                                            | cont7 == iterador[ii] | cont8 == iterador[ii] | cont9 == iterador[ii] | cont10 == iterador[ii]) {
                                                        jj++;
                                                    } else {
                                                        j = i;
                                                        i = h;
                                                        h = g;
                                                        g = f;
                                                        f = e;
                                                        e = d;
                                                        d = c;
                                                        c = b;
                                                        b = a;
                                                        a = jj;
                                                        jj++;
                                                    }
                                                }

                                                int dif1 = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(cont2).getPuntaje() + listaDeJugadores.get(cont3).getPuntaje()
                                                        + listaDeJugadores.get(cont4).getPuntaje() + listaDeJugadores.get(cont5).getPuntaje() + listaDeJugadores.get(cont6).getPuntaje()
                                                        + listaDeJugadores.get(cont7).getPuntaje() + listaDeJugadores.get(cont8).getPuntaje() + listaDeJugadores.get(cont9).getPuntaje()
                                                        + listaDeJugadores.get(cont10).getPuntaje())
                                                        - (listaDeJugadores.get(a).getPuntaje() + listaDeJugadores.get(b).getPuntaje() + listaDeJugadores.get(c).getPuntaje()
                                                        + listaDeJugadores.get(d).getPuntaje() + listaDeJugadores.get(e).getPuntaje() + listaDeJugadores.get(f).getPuntaje()
                                                        + listaDeJugadores.get(g).getPuntaje() + listaDeJugadores.get(h).getPuntaje() + listaDeJugadores.get(i).getPuntaje()
                                                        + listaDeJugadores.get(j).getPuntaje()));
                                                if (dif1 < dif) {
                                                    jugador1 = 0;
                                                    jugador2 = cont2;
                                                    jugador3 = cont3;
                                                    jugador4 = cont4;
                                                    jugador5 = cont5;
                                                    jugador6 = cont6;
                                                    jugador7 = cont7;
                                                    jugador8 = cont8;
                                                    jugador9 = cont9;
                                                    jugador10 = cont10;
                                                    jugador11 = a;
                                                    jugador12 = b;
                                                    jugador13 = c;
                                                    jugador14 = d;
                                                    jugador15 = e;
                                                    jugador16 = f;
                                                    jugador17 = g;
                                                    jugador18 = h;
                                                    jugador19 = i;
                                                    jugador20 = j;
                                                    dif = dif1;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            listaEquipoBlanco.add(listaDeJugadores.get(jugador1));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador2));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador3));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador4));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador5));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador6));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador7));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador8));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador9));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador10));
            listaEquipoNegro.add(listaDeJugadores.get(jugador11));
            listaEquipoNegro.add(listaDeJugadores.get(jugador12));
            listaEquipoNegro.add(listaDeJugadores.get(jugador13));
            listaEquipoNegro.add(listaDeJugadores.get(jugador14));
            listaEquipoNegro.add(listaDeJugadores.get(jugador15));
            listaEquipoNegro.add(listaDeJugadores.get(jugador16));
            listaEquipoNegro.add(listaDeJugadores.get(jugador17));
            listaEquipoNegro.add(listaDeJugadores.get(jugador18));
            listaEquipoNegro.add(listaDeJugadores.get(jugador19));
            listaEquipoNegro.add(listaDeJugadores.get(jugador20));

            Collections.sort(listaEquipoBlanco, new SortByScore());
            Collections.sort(listaEquipoNegro, new SortByScore());


            team1.setText("Equipo Blanco: " + (listaEquipoBlanco.get(0).getPuntaje() + listaEquipoBlanco.get(1).getPuntaje() + listaEquipoBlanco.get(2).getPuntaje()
                    + listaEquipoBlanco.get(3).getPuntaje() + listaEquipoBlanco.get(4).getPuntaje()+ listaEquipoBlanco.get(5).getPuntaje()+ listaEquipoBlanco.get(6).getPuntaje()
                    + listaEquipoBlanco.get(7).getPuntaje() + listaEquipoBlanco.get(8).getPuntaje() + listaEquipoBlanco.get(9).getPuntaje()));
            team2.setText("Equipo Negro: " + (listaEquipoNegro.get(0).getPuntaje() + listaEquipoNegro.get(1).getPuntaje() + listaEquipoNegro.get(2).getPuntaje()
                    + listaEquipoNegro.get(3).getPuntaje() + listaEquipoNegro.get(4).getPuntaje() + listaEquipoNegro.get(5).getPuntaje() + listaEquipoNegro.get(6).getPuntaje()
                    + listaEquipoNegro.get(7).getPuntaje() + listaEquipoNegro.get(8).getPuntaje() + listaEquipoNegro.get(9).getPuntaje()));
        }

        //Para 22 jugadores
        else if (listaDeJugadores.size() == 22) {
            int dif = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(1).getPuntaje() + listaDeJugadores.get(2).getPuntaje() + listaDeJugadores.get(3).getPuntaje()
                    + listaDeJugadores.get(4).getPuntaje() + listaDeJugadores.get(5).getPuntaje()+ listaDeJugadores.get(6).getPuntaje() + listaDeJugadores.get(7).getPuntaje()
                    + listaDeJugadores.get(8).getPuntaje() + listaDeJugadores.get(9).getPuntaje() + listaDeJugadores.get(10).getPuntaje())
                    - (listaDeJugadores.get(11).getPuntaje() + listaDeJugadores.get(12).getPuntaje() + listaDeJugadores.get(13).getPuntaje() + listaDeJugadores.get(14).getPuntaje()
                    + listaDeJugadores.get(15).getPuntaje() + listaDeJugadores.get(16).getPuntaje() + listaDeJugadores.get(17).getPuntaje() + listaDeJugadores.get(18).getPuntaje()
                    + listaDeJugadores.get(19).getPuntaje() + listaDeJugadores.get(20).getPuntaje() + listaDeJugadores.get(21).getPuntaje()));

            for (int cont2 = 1; cont2 < listaDeJugadores.size() - 4; cont2++) {
                for (int cont3 = cont2 + 1; cont3 < listaDeJugadores.size() - 3; cont3++) {
                    for (int cont4 = cont3 + 1; cont4 < listaDeJugadores.size() - 2; cont4++) {
                        for (int cont5 = cont4 + 1; cont5 < listaDeJugadores.size() - 1; cont5++) {
                            for (int cont6 = cont5 + 1; cont6 < listaDeJugadores.size(); cont6++) {
                                for (int cont7 = cont6 + 1; cont7 < listaDeJugadores.size(); cont7++) {
                                    for (int cont8 = cont7 + 1; cont8 < listaDeJugadores.size(); cont8++) {
                                        for (int cont9 = cont8 + 1; cont9 < listaDeJugadores.size(); cont9++) {
                                            for (int cont10 = cont9 + 1; cont10 < listaDeJugadores.size(); cont10++) {
                                                for (int cont11 = cont10 + 1; cont11 < listaDeJugadores.size(); cont11++) {
                                                    int iterador[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21};
                                                    int jj = 1;
                                                    for (int ii = 0; ii < iterador.length; ii++) {
                                                        if (cont2 == iterador[ii] | cont3 == iterador[ii] | cont4 == iterador[ii] | cont5 == iterador[ii] | cont6 == iterador[ii]
                                                                | cont7 == iterador[ii] | cont8 == iterador[ii] | cont9 == iterador[ii] | cont10 == iterador[ii] | cont11 == iterador[ii]) {
                                                            jj++;
                                                        } else {
                                                            i = k;
                                                            j = i;
                                                            i = h;
                                                            h = g;
                                                            g = f;
                                                            f = e;
                                                            e = d;
                                                            d = c;
                                                            c = b;
                                                            b = a;
                                                            a = jj;
                                                            jj++;
                                                        }
                                                    }

                                                    int dif1 = Math.abs((listaDeJugadores.get(0).getPuntaje() + listaDeJugadores.get(cont2).getPuntaje() + listaDeJugadores.get(cont3).getPuntaje()
                                                            + listaDeJugadores.get(cont4).getPuntaje() + listaDeJugadores.get(cont5).getPuntaje() + listaDeJugadores.get(cont6).getPuntaje()
                                                            + listaDeJugadores.get(cont7).getPuntaje() + listaDeJugadores.get(cont8).getPuntaje() + listaDeJugadores.get(cont9).getPuntaje()
                                                            + listaDeJugadores.get(cont10).getPuntaje() + listaDeJugadores.get(cont11).getPuntaje())
                                                            - (listaDeJugadores.get(a).getPuntaje() + listaDeJugadores.get(b).getPuntaje() + listaDeJugadores.get(c).getPuntaje()
                                                            + listaDeJugadores.get(d).getPuntaje() + listaDeJugadores.get(e).getPuntaje() + listaDeJugadores.get(f).getPuntaje()
                                                            + listaDeJugadores.get(g).getPuntaje() + listaDeJugadores.get(h).getPuntaje() + listaDeJugadores.get(i).getPuntaje()
                                                            + listaDeJugadores.get(j).getPuntaje() + listaDeJugadores.get(k).getPuntaje()));
                                                    if (dif1 < dif) {
                                                        jugador1 = 0;
                                                        jugador2 = cont2;
                                                        jugador3 = cont3;
                                                        jugador4 = cont4;
                                                        jugador5 = cont5;
                                                        jugador6 = cont6;
                                                        jugador7 = cont7;
                                                        jugador8 = cont8;
                                                        jugador9 = cont9;
                                                        jugador10 = cont10;
                                                        jugador11 = cont11;
                                                        jugador12 = a;
                                                        jugador13 = b;
                                                        jugador14 = c;
                                                        jugador15 = d;
                                                        jugador16 = e;
                                                        jugador17 = f;
                                                        jugador18 = g;
                                                        jugador19 = h;
                                                        jugador20 = i;
                                                        jugador21 = j;
                                                        jugador22 = k;
                                                        dif = dif1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            listaEquipoBlanco.add(listaDeJugadores.get(jugador1));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador2));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador3));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador4));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador5));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador6));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador7));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador8));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador9));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador10));
            listaEquipoBlanco.add(listaDeJugadores.get(jugador11));
            listaEquipoNegro.add(listaDeJugadores.get(jugador12));
            listaEquipoNegro.add(listaDeJugadores.get(jugador13));
            listaEquipoNegro.add(listaDeJugadores.get(jugador14));
            listaEquipoNegro.add(listaDeJugadores.get(jugador15));
            listaEquipoNegro.add(listaDeJugadores.get(jugador16));
            listaEquipoNegro.add(listaDeJugadores.get(jugador17));
            listaEquipoNegro.add(listaDeJugadores.get(jugador18));
            listaEquipoNegro.add(listaDeJugadores.get(jugador19));
            listaEquipoNegro.add(listaDeJugadores.get(jugador20));
            listaEquipoNegro.add(listaDeJugadores.get(jugador21));
            listaEquipoNegro.add(listaDeJugadores.get(jugador22));

            Collections.sort(listaEquipoBlanco, new SortByScore());
            Collections.sort(listaEquipoNegro, new SortByScore());


            team1.setText("Equipo Blanco: " + (listaEquipoBlanco.get(0).getPuntaje() + listaEquipoBlanco.get(1).getPuntaje() + listaEquipoBlanco.get(2).getPuntaje()
                    + listaEquipoBlanco.get(3).getPuntaje() + listaEquipoBlanco.get(4).getPuntaje()+ listaEquipoBlanco.get(5).getPuntaje()+ listaEquipoBlanco.get(6).getPuntaje()
                    + listaEquipoBlanco.get(7).getPuntaje() + listaEquipoBlanco.get(8).getPuntaje() + listaEquipoBlanco.get(9).getPuntaje()+ listaEquipoBlanco.get(10).getPuntaje()));
            team2.setText("Equipo Negro: " + (listaEquipoNegro.get(0).getPuntaje() + listaEquipoNegro.get(1).getPuntaje() + listaEquipoNegro.get(2).getPuntaje()
                    + listaEquipoNegro.get(3).getPuntaje() + listaEquipoNegro.get(4).getPuntaje() + listaEquipoNegro.get(5).getPuntaje() + listaEquipoNegro.get(6).getPuntaje()
                    + listaEquipoNegro.get(7).getPuntaje() + listaEquipoNegro.get(8).getPuntaje() + listaEquipoNegro.get(9).getPuntaje()+ listaEquipoNegro.get(10).getPuntaje()));
        }
    }



    public void apretarBotonResultado (View view) {

        mEditBlanco   = (EditText)findViewById(R.id.resultado_blanco);
        mEditNegro   = (EditText)findViewById(R.id.resultado_negro);


        if (mEditBlanco.getText().length() != 0 && mEditNegro.getText().length() != 0) {
            int resultado_blanco = Integer.valueOf(mEditBlanco.getText().toString());
            int resultado_negro = Integer.valueOf(mEditNegro.getText().toString());

            int puntajeBlanco;
            int puntajeNegro;

            if (resultado_blanco>resultado_negro){
                if (resultado_blanco>resultado_negro+2) {
                    puntajeBlanco = 2;
                    puntajeNegro = -2;
                }
                else{
                    puntajeBlanco = 1;
                    puntajeNegro = -1;
                }
                Toast.makeText(this, "Equipo Blanco suma " + puntajeBlanco + ", Equipo Negro resta " + puntajeNegro, Toast.LENGTH_LONG).show();
            }
            else if (resultado_blanco<resultado_negro){
                if (resultado_blanco<resultado_negro-2) {
                    puntajeBlanco = -2;
                    puntajeNegro = 2;
                }
                else {
                    puntajeBlanco = -1;
                    puntajeNegro = 1;
                }
                Toast.makeText(this, "Equipo Blanco resta " + puntajeBlanco + ", Equipo Negro suma " + puntajeNegro, Toast.LENGTH_LONG).show();
            }
            else {
                puntajeBlanco = 0;
                puntajeNegro = 0;
                Toast.makeText(this, "Los puntajes no se modificaron", Toast.LENGTH_LONG).show();

            }

//notar que el jugadorID es casualmente el mismo lugar en la posicion del array pero es un atributo, no es lo mismo.
            for (Jugador jugador : listaEquipoBlanco) {
                String jugadorID = jugador.getJugadorID().toString();
                int puntaje = jugador.getPuntaje();
                int partidosJugados = jugador.getPartidosJugados();

                DatabaseReference jugadorRef = mDatabase.child(jugadorID);
                Map<String, Object> jugadorUpdates = new HashMap<>();
                jugadorUpdates.put("puntaje", puntaje+puntajeBlanco);
                jugadorUpdates.put("partidosJugados", partidosJugados+1);
                jugadorRef.updateChildren(jugadorUpdates);
            }
            for (Jugador jugador : listaEquipoNegro) {
                String jugadorID = jugador.getJugadorID().toString();
                int puntaje = jugador.getPuntaje();
                int partidosJugados = jugador.getPartidosJugados();

                DatabaseReference jugadorRef = mDatabase.child(jugadorID);
                Map<String, Object> jugadorUpdates = new HashMap<>();
                jugadorUpdates.put("puntaje", puntaje+puntajeNegro);
                jugadorUpdates.put("partidosJugados", partidosJugados+1);
                jugadorRef.updateChildren(jugadorUpdates);
            }

        }
        else {
            Toast.makeText(this, "Falta cargar el resultado", Toast.LENGTH_SHORT).show();
            }
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        }
}

class SortByScore implements Comparator<Jugador>
{
    public int compare(Jugador a, Jugador b)
    {
        if(a.getPuntaje() < b.getPuntaje()) return 1;
        if(a.getPuntaje() == b.getPuntaje()) return 0;
        else return -1;    }
}