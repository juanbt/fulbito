package com.beta.fulbito;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juan Btesh on 07/10/2016.
 */
public class ListViewJugadoresAdapter extends BaseAdapter {
    private static final String TAG = "MyActivity";

    private List<String> mJugadoresID = new ArrayList<>();
    private List<Jugador> mJugadores = new ArrayList<>();

    private Context mContext;
    private CheckBox checkBoxNombreJugador ;
    private TextView textViewPuntajeJugador;
    private TextView textViewPartidosJugados;
    private ImageView imageViewFotoJugador;

    private String nombreJugadorActual;
    private Integer puntajeJugadorActual;
    private Integer partidosJugadosJugadorActual;
    private int fotoJugadorActual;

    public ListViewJugadoresAdapter(Context context,List<Jugador> listaDeJugadores ) {
        this.mJugadores = listaDeJugadores;
        this.mContext = context;
    }

        @Override
    public int getCount() {
        return mJugadores.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mJugadores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater miInfladorDeLayouts = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = miInfladorDeLayouts.inflate(R.layout.listview_jugadores_detalle, parent, false);
        checkBoxNombreJugador = (CheckBox) convertView.findViewById(R.id.checkBox_listView_jugadores);
        textViewPuntajeJugador = (TextView) convertView.findViewById(R.id.textview_puntaje_jugadores);
        textViewPartidosJugados = (TextView) convertView.findViewById(R.id.textView_partidosJugados);
        imageViewFotoJugador = (ImageView) convertView.findViewById(R.id.CircleImageView_fotoJugador);

        Jugador jugadorActual = mJugadores.get(position);
        nombreJugadorActual = jugadorActual.getNombre();
        puntajeJugadorActual = jugadorActual.getPuntaje();
        partidosJugadosJugadorActual = jugadorActual.getPartidosJugados();

        //ParseInt de la foto que la declare String
        Picasso.get().load(jugadorActual.getFoto()).into(imageViewFotoJugador);

        // obsoleto con picasso: =  Integer.parseInt(jugadorActual.getFoto());

        checkBoxNombreJugador.setText(nombreJugadorActual);
        textViewPuntajeJugador.setText(puntajeJugadorActual.toString());
        textViewPartidosJugados.setText(partidosJugadosJugadorActual.toString());
        // obsoleto con picasso: imageViewFotoJugador.setImageResource(fotoJugadorActual);

        checkBoxNombreJugador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = ((CheckBox)v).isChecked();
                Jugador jugador = mJugadores.get(position);
                if (isChecked){
                    MainActivity.listaDeJugadoresSeleccionados.add(jugador);
                }
                else {
                    MainActivity.listaDeJugadoresSeleccionados.remove(jugador);
                }
            }
        });
        return convertView;
    }

}
