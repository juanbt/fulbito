package com.beta.fulbito;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Juan Btesh on 12/10/2016.
 */
public class ListViewEquiposAdapter extends BaseAdapter {

    private Context context;
    private TextView textViewNombre;
    private TextView textViewPuntaje;
    private ImageView imageViewFotoJugador;

    private List<Jugador> listaDeJugadores;
    private String nombreJugadorActual;
    private Integer puntajeJugadorActual;
    private int fotoJugadorActual;


    public ListViewEquiposAdapter(Context context, List listaDeJugadores) {
        this.context = context;
        this.listaDeJugadores = listaDeJugadores;
    }

    @Override
    public int getCount() {
        return listaDeJugadores.size();
    }

    @Override
    public Object getItem(int position) {
        return this.listaDeJugadores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater miInfladorDeLayouts = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = miInfladorDeLayouts.inflate(R.layout.listview_equipos_detalle, parent, false);
        textViewNombre = (TextView) convertView.findViewById(R.id.textView_nombre_listView_equipo);
        textViewPuntaje = (TextView) convertView.findViewById(R.id.textView_puntaje_listView_equipo);
        imageViewFotoJugador = (ImageView) convertView.findViewById(R.id.CircleImageView_fotoJugador);

        Jugador jugadorActual = listaDeJugadores.get(position);
        nombreJugadorActual = jugadorActual.getNombre();
        puntajeJugadorActual = jugadorActual.getPuntaje();

        Picasso.get().load(jugadorActual.getFoto()).into(imageViewFotoJugador);

        //obsoleto con picasso: fotoJugadorActual = Integer.parseInt(jugadorActual.getFoto());

        textViewNombre.setText(nombreJugadorActual);
        textViewPuntaje.setText(puntajeJugadorActual.toString());

        // obsoleto con picasso: imageViewFotoJugador.setImageResource(fotoJugadorActual);

        return convertView;
    }

}
