package com.beta.fulbito;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listViewJugadores;
    private List<Jugador> listaDeJugadores;
    public static List <Jugador> listaDeJugadoresSeleccionados;
    private ListViewJugadoresAdapter listViewInvitadosAdapter;
    private static final String TAG = "MainActivity";
    private DatabaseReference mDatabase;
    private Context mContext;

    private List<Jugador> playersList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //esto esta bien. se declara arriba y se instancia en el OnCreate.
        mDatabase = FirebaseDatabase.getInstance().getReference().child("jugadores");

        listaDeJugadores = new ArrayList<>();
        listaDeJugadoresSeleccionados = new ArrayList<>();


        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Jugador jugador = dataSnapshot.getValue(Jugador.class);
                playersList.add(jugador);

                //Ordeno por puntaje primero y después PJ
                Collections.sort(playersList, new SortByScore());
                Collections.sort(playersList);

                llenarJugadores();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Jugador changedJugador = dataSnapshot.getValue(Jugador.class);
                playersList.add(changedJugador);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Jugador removedJugador = dataSnapshot.getValue(Jugador.class);
                playersList.remove(removedJugador);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "jugador:onCancelled", databaseError.toException());
                Toast.makeText(mContext, "Failed to load player.",
                        Toast.LENGTH_SHORT).show();
            }
        });

        //listViewJugadores = (ListView) findViewById(R.id.activity_main_listView_jugadores);
        //listViewInvitadosAdapter = new ListViewJugadoresAdapter(this,listaDeJugadores);
        //listViewJugadores.setAdapter(listViewInvitadosAdapter);

    }
    public void onStart () {
            super.onStart();
        llenarJugadores();

        }


    public void llenarJugadores(){
        listaDeJugadores = playersList;
        listViewJugadores = (ListView) findViewById(R.id.activity_main_listView_jugadores);
        listViewInvitadosAdapter = new ListViewJugadoresAdapter(this,listaDeJugadores);
        listViewJugadores.setAdapter(listViewInvitadosAdapter);

        /*listaDeJugadores.add(new Jugador(0,"Pata",62,60,R.drawable.pata));
        listaDeJugadores.add(new Jugador(0,"Beta",83,53,R.drawable.beta));
        listaDeJugadores.add(new Jugador(0,"Juano",62,48,R.drawable.juano));
        listaDeJugadores.add(new Jugador(0,"Guido",71,42,R.drawable.guido));
        listaDeJugadores.add(new Jugador(0,"Mateo S",49,42,R.drawable.mateo_s));
        listaDeJugadores.add(new Jugador(0,"Mateo B",57,41,R.drawable.mateo_b));
        listaDeJugadores.add(new Jugador(0,"Mati",87,35,R.drawable.mati));
        listaDeJugadores.add(new Jugador(0,"Garay",86,33,R.drawable.garay));
        listaDeJugadores.add(new Jugador(0,"Lucas G",54,31,R.drawable.lucas_g));
        listaDeJugadores.add(new Jugador(0,"Juanma",80,29,R.drawable.juanma));
        listaDeJugadores.add(new Jugador(0,"Toto",75,29,R.drawable.toto));
        listaDeJugadores.add(new Jugador(0,"Corri",56,29,R.drawable.corri));
        listaDeJugadores.add(new Jugador(0,"Nico A",69,18,R.drawable.nico_a));
        listaDeJugadores.add(new Jugador(0,"Augus",59,18,R.drawable.tato));
        listaDeJugadores.add(new Jugador(0,"Lucas B",40,17,R.drawable.lucas_b));
        listaDeJugadores.add(new Jugador(0,"Maxi",63,12,R.drawable.maxi));
        listaDeJugadores.add(new Jugador(0,"Smart",60,12,R.drawable.smart));
        listaDeJugadores.add(new Jugador(0,"Nacho G",72,11,R.drawable.nacho_g));
        listaDeJugadores.add(new Jugador(0,"Pica",68,11,R.drawable.pica));
        listaDeJugadores.add(new Jugador(0,"Chorgo",63,10,R.drawable.chorgo));
        listaDeJugadores.add(new Jugador(0,"Bocha",43,9,R.drawable.bocha));
        listaDeJugadores.add(new Jugador(0,"Alexis",55,7,R.drawable.alexis));
        listaDeJugadores.add(new Jugador(0,"Malcolm",42,7,R.drawable.malcolm));
        listaDeJugadores.add(new Jugador(0,"Gaita",73,6,R.drawable.gaita));
        listaDeJugadores.add(new Jugador(0,"Roma",62,6,R.drawable.roma));
        listaDeJugadores.add(new Jugador(0,"Rama",57,6,R.drawable.rama));
        listaDeJugadores.add(new Jugador(0,"Rulo",49,6,R.drawable.rulo));
        listaDeJugadores.add(new Jugador(0,"Andrés",74,5,R.drawable.andres));
        listaDeJugadores.add(new Jugador(0,"Uruguayo",64,5,R.drawable.uruguayo));
        listaDeJugadores.add(new Jugador(0,"Tertza",63,5,R.drawable.tertza));
        listaDeJugadores.add(new Jugador(0,"Pincha",62,5,R.drawable.pincha));
        listaDeJugadores.add(new Jugador(0,"Pietra",66,4,R.drawable.pietra));
        listaDeJugadores.add(new Jugador(0,"Manra",76,3,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Bola",73,3,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Joaco",69,3,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Timmy",65,3,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Lucas (facu)",64,3,R.drawable.lucas_facu));
        listaDeJugadores.add(new Jugador(0,"Toni",63,3,R.drawable.toni));
        listaDeJugadores.add(new Jugador(0,"Andy",62,3,R.drawable.andy));
        listaDeJugadores.add(new Jugador(0,"Gero",60,3,R.drawable.gero));
        listaDeJugadores.add(new Jugador(0,"Mati B",58,3,R.drawable.mati_b));
        listaDeJugadores.add(new Jugador(0,"Sebas SL",61,2,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Marcos",51,2,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Nano",87,1,R.drawable.nano));
        listaDeJugadores.add(new Jugador(0,"Lucas T",84,1,R.drawable.lucas_t));
        listaDeJugadores.add(new Jugador(0,"Peón",75,1,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Vico",73,1,R.drawable.vico));
        listaDeJugadores.add(new Jugador(0,"Pepo",71,1,R.drawable.pepo));
        listaDeJugadores.add(new Jugador(0,"Nacho B",68,1,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Lucas M",64,1,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Pefu",62,1,R.drawable.pefu));
        listaDeJugadores.add(new Jugador(0,"Lucas Suizo",62,1,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Esteban",62,1,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Lucas dC",62,1,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Pedro Furlong",61,1,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Nico B",58,1,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Enano",57,1,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Fede C",54,1,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Juan A",52,1,R.drawable.pelota));
        listaDeJugadores.add(new Jugador(0,"Santi B",45,1,R.drawable.pelota));
        */
    }

    public void apretarBotonArmarEquipos (View view) {
        int cantidadSeleccionada = listaDeJugadoresSeleccionados.size();
        if (cantidadSeleccionada%2!=0){
            Toast.makeText(this, "Debe seleccionar un número par de jugadores", Toast.LENGTH_SHORT).show();
        }
        else if (cantidadSeleccionada < 4){
            Toast.makeText(this, "Debe seleccionar por lo menos 4 jugadores", Toast.LENGTH_SHORT).show();
        }
        else if (cantidadSeleccionada > 22){
            Toast.makeText(this, "No puede seleccionar a más de 22 jugadores", Toast.LENGTH_SHORT).show();
        }
        else{
            Intent intent = new Intent(this,EquiposActivity.class);
            startActivity(intent);
        }
    }

}