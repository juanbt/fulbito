package com.beta.fulbito;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Juan Btesh on 07/10/2016.
 */
public class Jugador implements Comparable<Jugador> {
    private String nombre;
    private Integer jugadorID;
    private Integer puntaje;
    private Integer partidosJugados;
    private Integer puntajeMax;
    private Integer puntajeMin;
    private String foto;

    public Jugador (Integer jugadorID, String nombre, Integer puntaje, Integer partidosJugados, String foto){
        this.jugadorID = jugadorID;
        this.nombre = nombre;
        this.puntaje = puntaje;
        this.partidosJugados = partidosJugados;
        this.foto = foto;
    }

    public Jugador() {
        // Default constructor required for calls to DataSnapshot.getValue(Jugador.class)
    }


    private void agregarJugador (Integer jugadorID, String nombre, Integer puntaje, Integer partidosJugados, String foto) {
        Jugador jugador = new Jugador (jugadorID, nombre, puntaje, partidosJugados, foto);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(Integer puntaje) {
        this.puntaje = puntaje;
    }

    public Integer getPartidosJugados() {
        return partidosJugados;
    }

    public void setPartidosJugados(Integer partidosJugados) {
        this.partidosJugados = partidosJugados;
    }

    public void setFoto (String foto){this.foto = foto;}

    public String getFoto (){
        return foto;
    }

    public Integer getPuntajeMax() {
        return puntajeMax;
    }

    public void setPuntajeMax(Integer puntajeMax) {
        this.puntajeMax = puntajeMax;
    }

    public Integer getPuntajeMin() {
        return puntajeMin;
    }

    public void setPuntajeMin(Integer puntajeMin) {
        this.puntajeMin = puntajeMin;
    }

    public Integer getJugadorID() {return jugadorID;}

    public void setJugadorID(Integer jugadorID) { this.jugadorID = jugadorID; }

    @Override
    public int compareTo(@NonNull Jugador jugador) {
        if(this.getPartidosJugados() < jugador.getPartidosJugados()) return 1;
        if(this.getPartidosJugados() == jugador.getPartidosJugados()) return 0;
        else return -1;    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("foto", foto);
        result.put("jugadorID", jugadorID);
        result.put("nombre", nombre);
        result.put("partidosJugados", partidosJugados);
        result.put("puntaje", puntaje);
        return result;
    }

}
